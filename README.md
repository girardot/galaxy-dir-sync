# What it does

The script synchronises the content of a network directory (option ``-d /abs/path/to/dir``) to a specific folder of 
a Galaxy's Library (given with ``-l galaxy_libname`` and ``-f folder_name``). 
The network directory must also be accessible from the server running galaxy.
 
The directory sub-structure is also replicated in the Galaxy Library's folder and all 
files found in the directory sub-structure are added in the Galaxy's Library using symbolic links.
Compressed files with ``gz``, ``zip``, ``bz2`` extensions are properly handled. You can extend this list by adding missing 
extensions to the hard coded ``COMPRESSION_EXT_LIST`` list. In case you do this, make sure that your galaxy instance also properly handles 
file compressed with the new extensions. 

When ``-c`` is given (the default), the script first checks if all datasets listed in the galaxy library's folder (and sub-folders) 
still exist **in the synch'ed directory** (i.e. given by ``-d``); and datasets with broken links are removed from the Galaxy library. 

By default all files and folders are synchronised but files which name starts with a ``.``. 
Additional files and/or folders can be ignored using a ``.galaxy_ignore`` file placed in the synchronized directory (i.e. directly in ``-d`` path). 
The ``.galaxy_ignore`` file expects one pattern per line, each pattern will be matched against 
the entire file or directory name (i.e. not the path, just the name). The wildcard char ``*`` can be used to mean *any character 0 or more times* 
while any other characters are escaped and interprested as a normal character i.e. a ``.`` means *a dot* not *any letter*. 

For instance, all bam indices can be skipped by adding a ``*.bai`` expression in the ``.galaxy_ignore`` 

     
# Installation and dependencies
* Install parsec following the directions available at (https://parsec.readthedocs.io/en/latest/), this will install all bioblend and yaml deps
	* Make sure to run ``parsec init`` to register your galaxy url and key, this will make your life easier
* Alternatively:
   * if you have conda : 
`conda create -n parsec python=2.7` 
then 
`source activate parsec && conda install galaxy-parsec`
   * make sure pyyaml and bioblend are available
* Copy the ``galaxy-dir-sync.py`` script file
* that's it
 
  
  
# Documentation

* run with ``--help``

The script takes the following mandatory args : 
* ``-d`` : Absolute path to directory to sync with galaxy
* ``-l`` : Name of the Galaxy Library. It should be already existing and you should have write permissions on it.
* ``-f`` : Name of the folder to be used in the specified Galaxy Library. Will be created if not existing.

Mandatory if you haven't properly initialized parsec (or you want to connect to another galaxy instance)
* ``-u`` : URL of the galaxy instance ; default to the value you gave at parsec init
* ``-k`` : Your API key (to be first created in galaxy under the ``User > Preferences > Manage API key `` menu) ; default to the value you gave at parsec init

Optional :
* ``-c`` : check for broken links (files registered in galaxy but files not found on disk)
* ``--daemon`` and ``-s`` : run in daemon mode with a rest time of ``-s`` seconds between each round
* ``-v`` : verbose level, see help
* ``--dryrun`` : don't run for real, just prints what the script would do


